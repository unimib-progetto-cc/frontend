# Link: https://vuejs.org/v2/cookbook/dockerize-vuejs-app.html
# build stage
FROM node:lts-alpine as build-stage

ENV VUE_APP_USERS_ENDPOINT=http://192.168.49.2:32501
ENV VUE_APP_CARS_ENDPOINT=http://192.168.49.2:32502
ENV VUE_APP_BOOKINGS_ENDPOINT=http://192.168.49.2:32503
ENV VUE_APP_SENSORS_ENDPOINT=http://192.168.49.2:32504

WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

# production stage
FROM nginx:stable-alpine as production-stage
COPY --from=build-stage /app/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]