import Vue from 'vue'
import Vuex from 'vuex'

import auth from './stores/auth.js'
import bookings from './stores/bookings.js'
import cars from './stores/cars.js'
import customers from './stores/customers.js'
import sensors from './stores/sensors.js'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    auth,
    bookings,
    cars,
    customers,
    sensors
  },

  getters: {
    isLoading: (state, getters) => getters['auth/isLoading'] || getters['bookings/isLoading'] || getters['cars/isLoading'] || getters['customers/isLoading'] || getters['sensors/isLoading']
  }
})

export default store
