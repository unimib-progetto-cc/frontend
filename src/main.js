import Vue from 'vue'
import axios from 'axios'

import App from './App.vue'
import config from './config.js'
import router from './router.js'
import store from './store.js'

Vue.prototype.$auth = axios.create({ baseURL: config.endpoints.auth })
Vue.prototype.$cars = axios.create({ baseURL: config.endpoints.cars })
Vue.prototype.$users = axios.create({ baseURL: config.endpoints.users })
Vue.prototype.$bookings = axios.create({ baseURL: config.endpoints.bookings })
Vue.prototype.$sensors = axios.create({ baseURL: config.endpoints.sensors })

Vue.config.productionTip = config.vue.productionTip

new Vue({
  render: h => h(App),
  store,
  router,
  beforeCreate () {
    this.$store.dispatch('auth/restoreSession')
  }
}).$mount('#app')
