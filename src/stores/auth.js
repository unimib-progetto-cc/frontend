import router from '@/router.js'

const auth = {
  namespaced: true,

  state: {
    isLoggingIn: false,
    loginError: null,

    isRegistering: false,
    registrationError: null,
    registrationSuccess: null,

    token: window.localStorage.getItem('token') || null,

    isFetchingProfile: false,
    item: {
      id: null,
      name: null,
      surname: null,
      email: null
    }
  },

  mutations: {
    profileStart (state) {
      state.isFetchingProfile = true
    },
    profileSuccess (state, { name, surname, email }) {
      state.isFetchingProfile = false
      state.item = { name, surname, email }
    },
    profileError (state) {
      state.isFetchingProfile = false
      state.item = {
        id: null,
        name: null,
        surname: null,
        email: null
      }
    },

    loginStart (state) {
      state.isLoggingIn = true
      state.loginError = null
    },
    loginSuccess (state, token) {
      state.isLoggingIn = false

      state.token = token
    },
    loginError (state, message) {
      state.isLoggingIn = false
      state.loginError = message
    },

    registrationStart (state) {
      state.isRegistering = true
      state.registrationError = null
      state.registrationSuccess = null
    },
    registrationSuccess (state, message) {
      state.isRegistering = false
      state.registrationSuccess = message
    },
    registrationError (state, message) {
      state.isRegistering = false
      state.registrationError = message
    },

    logout (state) {
      state.token = null
    }
  },

  actions: {
    fetchProfile (store) {
      store.commit('profileStart')

      this._vm.$auth.get('/auth/profile', { headers: { Authorization: `Bearer ${store.state.token}` } })
        .then((response) => { store.commit('profileSuccess', response.data) })
        .catch(() => store.commit('profileError'))
    },

    restoreSession (store) {
      const token = window.localStorage.getItem('token', undefined)
      if (token === undefined) {
        store.dispatch('logout')
        return
      }

      store.commit('loginSuccess', token)
      store.dispatch('registerAuthHandlers', token)
      store.dispatch('fetchProfile', token)
    },

    login (store, credentials) {
      store.commit('loginStart')

      return new Promise((resolve, reject) => {
        this._vm.$auth.post('/login', credentials)
          .then((response) => {
            const token = response.headers.authorization.substr(7)

            window.localStorage.setItem('token', token)
            store.commit('loginSuccess', token)
            store.dispatch('registerAuthHandlers', token)
            store.dispatch('fetchProfile', token)

            resolve(token)
          })
          .catch((error) => {
            let message = '<strong>Invalid credentials:</strong> make sure the provided username and password are correct!'
            if (!error.response.status) {
              message = '<strong>Network error:</strong> make sure that the authentication microservice is reachable!'
            }

            store.commit('loginError', message)

            reject(error)
          })
      })
    },

    register (store, formData) {
      store.commit('registrationStart')

      return new Promise((resolve, reject) => {
        this._vm.$auth.post('/auth/register', formData)
          .then((response) => {
            const message = 'You signed up successfully. You will be redirected to the login page in a few seconds...'
            store.commit('registrationSuccess', message)

            resolve(response)
          })
          .catch((error) => {
            store.commit('registrationError', error.response.data.message)

            reject(error)
          })
      })
    },

    registerAuthHandlers (store, token) {
      const errorHandler = (error) => {
        if ('status' in error.response && error.response.status === 401) {
          store.dispatch('logout')
        }

        return Promise.reject(error)
      }

      this._vm.$cars.defaults.headers.Authorization = `Bearer ${token}`
      this._vm.$cars.interceptors.response.use(null, errorHandler)
      this._vm.$users.defaults.headers.Authorization = `Bearer ${token}`
      this._vm.$users.interceptors.response.use(null, errorHandler)
      this._vm.$bookings.defaults.headers.Authorization = `Bearer ${token}`
      this._vm.$bookings.interceptors.response.use(null, errorHandler)
      this._vm.$sensors.defaults.headers.Authorization = `Bearer ${token}`
      this._vm.$sensors.interceptors.response.use(null, errorHandler)
    },

    logout (store) {
      window.localStorage.removeItem('token')
      store.commit('logout')

      this._vm.$cars.defaults.headers.Authorization = null
      this._vm.$cars.interceptors.response.eject(0)
      this._vm.$users.defaults.headers.Authorization = null
      this._vm.$users.interceptors.response.eject(0)
      this._vm.$bookings.defaults.headers.Authorization = null
      this._vm.$bookings.interceptors.response.eject(0)
      this._vm.$sensors.defaults.headers.Authorization = null
      this._vm.$sensors.interceptors.response.eject(0)

      router.push({ name: 'login' })
    }
  },

  getters: {
    isLoading: (state) => state.isLoggingIn || state.isRegistering || state.isFetchingProfile,

    user: (state) => state.item,

    hasLoginErrors: (state) => state.loginError != null,
    loginMessage: (state) => state.loginError,

    hasRegistrationMessage: (state) => state.registrationSuccess != null || state.registrationError != null,
    registrationMessage: (state) => state.registrationSuccess || state.registrationError,
    registrationMessageType: (state) => {
      if (state.registrationSuccess != null) {
        return 'success'
      }

      return 'error'
    }
  }
}

export default auth
