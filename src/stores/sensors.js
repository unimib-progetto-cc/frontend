const sensors = {
  namespaced: true,

  state: {
    fields: [
      { key: 'id', label: 'Id' },
      { key: 'carId', label: 'Car' },
      { key: 'latitude', label: 'Latitude', render: (row) => row.position.latitude },
      { key: 'longitude', label: 'Longitude', render: (row) => row.position.longitude },
      {
        key: 'weather-condition',
        label: 'Weather Condition',
        render: (row) => {
          const icons = {
            'clear sky': '01d@2x.png',
            'thunderstorm with light rain': '11d@2x.png',
            'thunderstorm with rain': '11d@2x.png',
            'thunderstorm with heavy rain': '11d@2x.png',
            'light thunderstorm': '11d@2x.png',
            thunderstorm: '11d@2x.png',
            'heavy thunderstorm': '11d@2x.png',
            'ragged thunderstorm': '11d@2x.png',
            'thunderstorm with light drizzle': '11d@2x.png',
            'thunderstorm with drizzle': '11d@2x.png',
            'thunderstorm with heavy drizzle': '11d@2x.png',
            'light intensity drizzle': '09d@2x.png',
            drizzle: '09d@2x.png',
            'heavy intensity drizzle': '09d@2x.png',
            'light intensity drizzle rain': '09d@2x.png',
            'drizzle rain': '09d@2x.png',
            'heavy intensity drizzle rain': '09d@2x.png',
            'shower rain and drizzle': '09d@2x.png',
            'heavy shower rain and drizzle': '09d@2x.png',
            'shower drizzle': '09d@2x.png',
            'light rain': '10d@2x.png',
            'moderate rain': '10d@2x.png',
            'heavy intensity rain': '10d@2x.png',
            'very heavy rain': '10d@2x.png',
            'extreme rain': '10d@2x.png',
            'freezing rain': '13d@2x.png',
            'light intensity shower rain': '09d@2x.png',
            'shower rain': '09d@2x.png',
            'heavy intensity shower rain': '09d@2x.png',
            'ragged shower rain': '09d@2x.png',
            'light snow': '13d@2x.png',
            'heavy snow': '13d@2x.png',
            sleet: '13d@2x.png',
            'light shower sleet': '13d@2x.png',
            'shower sleet': '13d@2x.png',
            'light rain and snow': '13d@2x.png',
            'rain and snow': '13d@2x.png',
            'light shower snow': '13d@2x.png',
            'shower snow': '13d@2x.png',
            'heavy shower snow': '13d@2x.png',
            'few clouds': '02d@2x.png',
            'scattered clouds': '03d@2x.png',
            'broken clouds': '04d@2x.png',
            'overcast clouds': '04d@2x.png',
            snow: '13d@2x.png',
            mist: '50d@2x.png',
            smoke: '50d@2x.png',
            haze: '50d@2x.png',
            'sand/ dust whirls': '50d@2x.png',
            fog: '50d@2x.png',
            sand: '50d@2x.png',
            dust: '50d@2x.png',
            'volcanic ash': '50d@2x.png',
            squalls: '50d@2x.png',
            tornado: '50d@2x.png'
          }

          return `<img src="http://openweathermap.org/img/wn/${icons[row.weather.description.toLowerCase()]}" width="32" height="32" /><span>${row.weather.description}</span>`
        }
      },
      { key: 'temperature', label: 'Temperature', render: (row) => `${row.weather.temperature} °C` }
    ],

    isFetchingList: false,
    fetchListError: null,
    list: [],

    isFetchingSingle: false,
    fetchSingleError: null,
    entity: {},

    isCreating: false,
    createError: null
  },

  mutations: {
    fetchListStart (state) {
      state.isFetchingList = true
    },
    fetchListSuccess (state, sensors) {
      state.list = sensors

      state.fetchListError = null
      state.isFetchingList = false
    },
    fetchListError (state, error) {
      state.list = []

      state.fetchListError = error
      state.isFetchingList = false
    },

    fetchSingleStart (state) {
      state.isFetchingSingle = true
    },
    fetchSingleSuccess (state, response) {
      state.entity = response

      state.fetchSingleError = null
      state.isFetchingSingle = false
    },
    fetchSingleError (state, error) {
      state.entity = {}

      state.fetchSingleError = error
      state.isFetchingSingle = false
    },

    createStart (state) {
      state.isCreating = true
      state.createError = null
    },
    createSuccess (state) {
      state.isCreating = false
    },
    createError (state, message) {
      state.isCreating = false
      state.createError = message
    }
  },

  actions: {
    fetchList (store) {
      store.commit('fetchListStart')

      this._vm.$sensors.get('/')
        .then((response) => store.commit('fetchListSuccess', response.data.sensors))
        .catch((error) => store.commit('fetchListError', error.response))
    },
    fetchListByCar (store, car) {
      store.commit('fetchListStart')

      this._vm.$sensors.get('/', { params: { carId: car } })
        .then((response) => store.commit('fetchListSuccess', response.data.sensors))
        .catch((error) => store.commit('fetchListError', error.response))
    },

    fetchSingle (store, id) {
      store.commit('fetchSingleStart')

      this._vm.$sensors.get(`/${id}`)
        .then((response) => store.commit('fetchSingleSuccess', response.data))
        .catch((error) => store.commit('fetchSingleError', error.response))
    },

    create (store, data) {
      store.commit('createStart')

      return new Promise((resolve, reject) => {
        this._vm.$sensors.post('/', data)
          .then((response) => {
            store.commit('createSuccess', response.data)
            resolve(response)
          })
          .catch((error) => {
            store.commit('createError', error.response.data)
            reject(error)
          })
      })
    }
  },

  getters: {
    isLoading: (state) => state.isFetchingList || state.isFetchingSingle || state.isCreating,
    fields: (state) => state.fields,

    items: (state) => state.list,

    item: (state) => state.entity,

    hasCreateError: (state) => state.createError != null,
    createError: (state) => state.createError
  }
}

export default sensors
