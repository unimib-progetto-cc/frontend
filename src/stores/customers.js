const customers = {
  namespaced: true,

  state: {
    fields: [
      { key: 'id', label: 'ID' },
      { key: 'name', label: 'Name' },
      { key: 'surname', label: 'Surname' },
      { key: 'email', label: 'Email' }
    ],

    isFetchingList: false,
    fetchListError: null,
    list: [],

    isFetchingSingle: false,
    fetchSingleError: null,
    entity: {},

    isCreating: false,
    createError: null,

    isUpdating: false,
    updateError: null,

    isDeleting: false,
    deleteError: null
  },

  mutations: {
    fetchListStart (state) {
      state.isFetchingList = true
    },
    fetchListSuccess (state, users) {
      state.list = users

      state.fetchListError = null
      state.isFetchingList = false
    },
    fetchListError (state, error) {
      state.list = []

      state.fetchListError = error
      state.isFetchingList = false
    },

    fetchSingleStart (state) {
      state.isFetchingSingle = true
    },
    fetchSingleSuccess (state, response) {
      state.entity = response

      state.fetchSingleError = null
      state.isFetchingSingle = false
    },
    fetchSingleError (state, error) {
      state.entity = {}

      state.fetchSingleError = error
      state.isFetchingSingle = false
    },

    createStart (state) {
      state.isCreating = true
      state.createError = null
    },
    createSuccess (state) {
      state.isCreating = false
    },
    createError (state, message) {
      state.isCreating = false
      state.createError = message
    },

    updateStart (state) {
      state.isUpdating = true
      state.updateError = null
    },
    updateSuccess (state) {
      state.isUpdating = false
    },
    updateError (state, message) {
      state.isUpdating = false
      state.updateError = message
    },

    deleteStart (state) {
      state.isDeleting = true
      state.deleteError = null
    },
    deleteSuccess (state) {
      state.isDeleting = false
    },
    deleteError (state, message) {
      state.isDeleting = false
      state.deleteError = message
    }
  },

  actions: {
    fetchList (store) {
      store.commit('fetchListStart')

      this._vm.$users.get('/')
        .then((response) => store.commit('fetchListSuccess', response.data.users))
        .catch((error) => store.commit('fetchListError', error.response))
    },

    fetchSingle (store, id) {
      store.commit('fetchSingleStart')

      this._vm.$users.get(`/${id}`)
        .then((response) => store.commit('fetchSingleSuccess', response.data))
        .catch((error) => store.commit('fetchSingleError', error.response))
    },

    create (store, data) {
      store.commit('createStart')

      return new Promise((resolve, reject) => {
        this._vm.$users.post('/', data)
          .then((response) => {
            store.commit('createSuccess', response.data)
            resolve(response)
          })
          .catch((error) => {
            store.commit('createError', error.response)
            reject(error)
          })
      })
    },

    update (store, { id, ...resource }) {
      store.commit('updateStart')

      return new Promise((resolve, reject) => {
        this._vm.$users.put(`/${id}`, resource)
          .then((response) => {
            store.commit('updateSuccess')
            resolve(response)
          })
          .catch((error) => {
            store.commit('updateError', error.response)
            reject(error)
          })
      })
    },

    delete (store, id) {
      store.commit('deleteStart')

      return new Promise((resolve, reject) => {
        this._vm.$users.delete(`/${id}`)
          .then((response) => {
            store.commit('deleteSuccess')
            resolve(response)
          })
          .catch((error) => {
            store.commit('deleteError', error.response)
            reject(error)
          })
      })
    }
  },

  getters: {
    isLoading: (state) => state.isFetchingList || state.isFetchingSingle || state.isCreating || state.isUpdating || state.isDeleting,

    fields: (state) => state.fields,
    items: (state) => state.list,

    item: (state) => state.entity,

    hasCreateError: (state) => state.createError != null,
    createError: (state) => state.createError,

    hasUpdateError: (state) => state.updateError != null,
    updateError: (state) => state.updateError,

    hasDeleteError: (state) => state.deleteError != null,
    deleteError: (state) => state.deleteError
  }
}

export default customers
