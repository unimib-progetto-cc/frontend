import Vue from 'vue'
import Router from 'vue-router'

import Login from '@/pages/Login.vue'
import Register from '@/pages/Register.vue'
import Dashboard from '@/pages/Dashboard.vue'

import Home from '@/pages/dashboard/Home.vue'
import ResourceWrapper from '@/pages/dashboard/ResourceWrapper.vue'

import CarsList from '@/pages/dashboard/cars/List.vue'
import CarsCreate from '@/pages/dashboard/cars/Create.vue'
import CarsDeails from '@/pages/dashboard/cars/Details.vue'
import CarsEdit from '@/pages/dashboard/cars/Edit.vue'

import CustomersList from '@/pages/dashboard/customers/List.vue'
import CustomersCreate from '@/pages/dashboard/customers/Create.vue'
import CustomersDeails from '@/pages/dashboard/customers/Details.vue'
import CustomersEdit from '@/pages/dashboard/customers/Edit.vue'

import BookingsList from '@/pages/dashboard/bookings/List.vue'
import BookingsCreate from '@/pages/dashboard/bookings/Create.vue'
import BookingsDeails from '@/pages/dashboard/bookings/Details.vue'
import BookingsEdit from '@/pages/dashboard/bookings/Edit.vue'

import SensorsList from '@/pages/dashboard/sensors/List.vue'
import SensorsCreate from '@/pages/dashboard/sensors/Create.vue'
import SensorsDeails from '@/pages/dashboard/sensors/Details.vue'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/auth/login',
      name: 'login',
      component: Login,
      meta: { guest: true }
    },
    {
      path: '/auth/register',
      name: 'register',
      component: Register,
      meta: { guest: true }
    },
    {
      path: '/dashboard',
      component: Dashboard,
      meta: {
        secured: true,
        label: 'Dashboard'
      },
      children: [
        {
          path: '',
          name: 'dashboard.index',
          component: Home,
          meta: { label: 'Home' }
        },
        {
          path: 'cars',
          component: ResourceWrapper,
          meta: { label: 'Cars' },
          children: [
            {
              path: '',
              name: 'cars.index',
              component: CarsList,
              meta: { label: 'List' }
            },
            {
              path: 'create',
              name: 'cars.create',
              component: CarsCreate,
              meta: { label: 'Create' }
            },
            {
              path: ':id',
              name: 'cars.details',
              component: CarsDeails,
              meta: { label: 'Details' }
            },
            {
              path: ':id/edit',
              name: 'cars.edit',
              component: CarsEdit,
              meta: { label: 'Edit' }
            }
          ]
        },
        {
          path: 'customers',
          component: ResourceWrapper,
          meta: { label: 'Customers' },
          children: [
            {
              path: '',
              name: 'customers.index',
              component: CustomersList,
              meta: { label: 'List' }
            },
            {
              path: 'create',
              name: 'customers.create',
              component: CustomersCreate,
              meta: { label: 'Create' }
            },
            {
              path: ':id',
              name: 'customers.details',
              component: CustomersDeails,
              meta: { label: 'Details' }
            },
            {
              path: ':id/edit',
              name: 'customers.edit',
              component: CustomersEdit,
              meta: { label: 'Edit' }
            }
          ]
        },
        {
          path: 'bookings',
          component: ResourceWrapper,
          meta: { label: 'Bookings' },
          children: [
            {
              path: '',
              name: 'bookings.index',
              component: BookingsList,
              meta: { label: 'List' }
            },
            {
              path: 'create',
              name: 'bookings.create',
              component: BookingsCreate,
              meta: { label: 'Create' }
            },
            {
              path: ':id',
              name: 'bookings.details',
              component: BookingsDeails,
              meta: { label: 'Details' }
            },
            {
              path: ':id/edit',
              name: 'bookings.edit',
              component: BookingsEdit,
              meta: { label: 'Edit' }
            }
          ]
        },
        {
          path: 'sensors',
          component: ResourceWrapper,
          meta: { label: 'Sensor Recordings' },
          children: [
            {
              path: '',
              name: 'sensors.index',
              component: SensorsList,
              meta: { label: 'List' }
            },
            {
              path: 'create',
              name: 'sensors.create',
              component: SensorsCreate,
              meta: { label: 'Create' }
            },
            {
              path: ':id',
              name: 'sensors.details',
              component: SensorsDeails,
              meta: { label: 'Details' }
            }
          ]
        }
      ]
    },
    {
      path: '*',
      redirect: '/dashboard'
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.secured)) {
    if (window.localStorage.getItem('token') == null && to.name !== 'login') {
      next({
        name: 'login',
        params: { nextUrl: to.fullPath }
      })
      return
    }
  }

  if (to.matched.some(record => record.meta.guest)) {
    if (window.localStorage.getItem('token') != null) {
      next({ name: 'dashboard' })
      return
    }
  }

  next()
})

export default router
