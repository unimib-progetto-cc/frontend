const config = {
  endpoints: {
    auth: process.env.VUE_APP_USERS_ENDPOINT,
    cars: process.env.VUE_APP_CARS_ENDPOINT + '/cars',
    users: process.env.VUE_APP_USERS_ENDPOINT + '/users',
    bookings: process.env.VUE_APP_BOOKINGS_ENDPOINT + '/bookings',
    sensors: process.env.VUE_APP_SENSORS_ENDPOINT + '/sensors'
  },

  vue: {
    productionTip: false
  },

  styles: {
    buttons: {
      primary: 'w-full py-3 mt-2 bg-gray-800 rounded font-medium text-white uppercase focus:outline-none hover:bg-gray-700 hover:shadow-none disabled:bg-gray-200 transition'
    },
    forms: {
      label: 'block text-xs font-semibold text-gray-560 uppercase tracking-wide',
      input: 'block w-full py-2 px-3 mt-2 mb-4 rounded text-gray-700 bg-white border-2 border-gray-200 focus:outline-none focus:border-gray-800 transition disabled:cursor-pointer'
    }
  }
}

export default config
